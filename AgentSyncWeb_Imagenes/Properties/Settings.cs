﻿using AgentSyncWebImagenes.Clases;

namespace AgentSyncWebImagenes.Properties
{

    // NOTA IMPORTANTE: LUEGO DE HACER CAMBIOS EN LOS SETTINGS, EL ARCHIVO SETTINGS.DESIGNER.CS
    // SE REGENERA CON LOS NUEVOS VALORES, SIN EMBARGO PARA QUE EL PROGRAMA SE EJECUTE Y
    // NO DE ERROR AL COMPILAR / EJECUTAR, DEBEMOS IR A ESE ARCHIVO Y CAMBIAR LA SIGUIENTE LINEA:
    // internal sealed partial class Settings : global::System.Configuration.ApplicationSettingsBase {
    // CAMBIAR LA PALABRA internal por public
    // y Listo.

    // This class allows you to handle specific events on the settings class:
    //  The SettingChanging event is raised before a setting's value is changed.
    //  The PropertyChanged event is raised after a setting's value is changed.
    //  The SettingsLoaded event is raised after the setting values are loaded.
    //  The SettingsSaving event is raised before the setting values are saved.
    public sealed partial class Settings
    {

        public Settings()
        {
            // // To add event handlers for saving and changing settings, uncomment the lines below:
            //
            // this.SettingChanging += this.SettingChangingEventHandler;
            //
            this.SettingsSaving += this.SettingsSavingEventHandler;
            //
        }

        private void SettingChangingEventHandler(object sender, System.Configuration.SettingChangingEventArgs e)
        {
            // Add code to handle the SettingChangingEvent event here.
        }

        private void SettingsSavingEventHandler(object sender, System.ComponentModel.CancelEventArgs e)
        {
            // Add code to handle the SettingsSaving event here.

            foreach (System.Configuration.SettingsPropertyValue Item in this.PropertyValues)
            {
                if (Item.IsDirty)
                {
                    Item.KeepValue();
                    Item.IsDirty = false;
                }
            }

            e.Cancel = true;
        }
    }
}
