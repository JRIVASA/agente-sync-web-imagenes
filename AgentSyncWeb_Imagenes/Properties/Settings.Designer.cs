﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18444
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AgentSyncWebImagenes.Properties {
    
    
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "12.0.0.0")]
    public sealed partial class Settings : global::System.Configuration.ApplicationSettingsBase {
        
        private static Settings defaultInstance = ((Settings)(global::System.Configuration.ApplicationSettingsBase.Synchronized(new Settings())));
        
        public static Settings Default {
            get {
                return defaultInstance;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("")]
        public string Sucursal {
            get {
                return ((string)(this["Sucursal"]));
            }
            set {
                this["Sucursal"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("")]
        public string Deposito {
            get {
                return ((string)(this["Deposito"]));
            }
            set {
                this["Deposito"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("Bigwise\\Stellar\\")]
        public string ProgramDataDir {
            get {
                return ((string)(this["ProgramDataDir"]));
            }
            set {
                this["ProgramDataDir"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("AgentSyncWebImagenes")]
        public string ApplicationName {
            get {
                return ((string)(this["ApplicationName"]));
            }
            set {
                this["ApplicationName"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("AgentSyncWeb-Credentials-MinimumSecurity. 083074228330267121212442327883534435435" +
            "")]
        public string EncryptionKey {
            get {
                return ((string)(this["EncryptionKey"]));
            }
            set {
                this["EncryptionKey"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("")]
        public string RemoteDB_SQLServerName {
            get {
                return ((string)(this["RemoteDB_SQLServerName"]));
            }
            set {
                this["RemoteDB_SQLServerName"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("")]
        public string RemoteDB_SQLDBName {
            get {
                return ((string)(this["RemoteDB_SQLDBName"]));
            }
            set {
                this["RemoteDB_SQLDBName"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("")]
        public string RemoteDB_SQLUser {
            get {
                return ((string)(this["RemoteDB_SQLUser"]));
            }
            set {
                this["RemoteDB_SQLUser"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("")]
        public string RemoteDB_SQLPass {
            get {
                return ((string)(this["RemoteDB_SQLPass"]));
            }
            set {
                this["RemoteDB_SQLPass"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("")]
        public string LocalDB_SQLServerName {
            get {
                return ((string)(this["LocalDB_SQLServerName"]));
            }
            set {
                this["LocalDB_SQLServerName"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("")]
        public string LocalDB_SQLDBName {
            get {
                return ((string)(this["LocalDB_SQLDBName"]));
            }
            set {
                this["LocalDB_SQLDBName"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("")]
        public string LocalDB_SQLUser {
            get {
                return ((string)(this["LocalDB_SQLUser"]));
            }
            set {
                this["LocalDB_SQLUser"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("")]
        public string LocalDB_SQLPass {
            get {
                return ((string)(this["LocalDB_SQLPass"]));
            }
            set {
                this["LocalDB_SQLPass"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("False")]
        public bool LocalDB_WindowsAuth {
            get {
                return ((bool)(this["LocalDB_WindowsAuth"]));
            }
            set {
                this["LocalDB_WindowsAuth"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("100")]
        public int PorcentajeExistencia {
            get {
                return ((int)(this["PorcentajeExistencia"]));
            }
            set {
                this["PorcentajeExistencia"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("")]
        public string FTP_User {
            get {
                return ((string)(this["FTP_User"]));
            }
            set {
                this["FTP_User"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("")]
        public string FTP_Pwd {
            get {
                return ((string)(this["FTP_Pwd"]));
            }
            set {
                this["FTP_Pwd"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("")]
        public string FTP_Dir {
            get {
                return ((string)(this["FTP_Dir"]));
            }
            set {
                this["FTP_Dir"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("")]
        public string FTP_Host {
            get {
                return ((string)(this["FTP_Host"]));
            }
            set {
                this["FTP_Host"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("")]
        public string FTP_Port {
            get {
                return ((string)(this["FTP_Port"]));
            }
            set {
                this["FTP_Port"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("False")]
        public bool FTP_Auto {
            get {
                return ((bool)(this["FTP_Auto"]));
            }
            set {
                this["FTP_Auto"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("")]
        public string FTP_AutoInstance {
            get {
                return ((string)(this["FTP_AutoInstance"]));
            }
            set {
                this["FTP_AutoInstance"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("")]
        public string FTP_AutoDB {
            get {
                return ((string)(this["FTP_AutoDB"]));
            }
            set {
                this["FTP_AutoDB"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("")]
        public string FTP_ProcessURL {
            get {
                return ((string)(this["FTP_ProcessURL"]));
            }
            set {
                this["FTP_ProcessURL"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("")]
        public string FTP_UploadContentBaseDir {
            get {
                return ((string)(this["FTP_UploadContentBaseDir"]));
            }
            set {
                this["FTP_UploadContentBaseDir"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("")]
        public string RutaArchivo {
            get {
                return ((string)(this["RutaArchivo"]));
            }
            set {
                this["RutaArchivo"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("")]
        public string RutaImgWeb {
            get {
                return ((string)(this["RutaImgWeb"]));
            }
            set {
                this["RutaImgWeb"] = value;
            }
        }
    }
}
