﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AgentSyncWebImagenes.Clases;

namespace AgentSyncWebImagenes
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Functions.LoadSettings();

            Database_Sync_Classes.DBSync.UploadData();

            System.Environment.Exit(0);

        }

    }
}
