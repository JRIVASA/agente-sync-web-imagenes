﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.VisualBasic;
using AgentSyncWebImagenes.Clases;

namespace AgentSyncWebImagenes
{
    static class Program
    {

        public static FSLogger Logger = new FSLogger(System.Environment.CurrentDirectory + "\\Log.txt");

        public static long gCodProducto = 859;
        public static String gNombreProducto = "Stellar BUSINESS";
        public static String gPK;

        /* Advertencia: NO Cambiar ninguno de los 3 parámetros anteriores. Para efectos de resguardo de las
        credenciales de acceso a SQL se esta utilizando la misma combinación de parámetros de Stellar BUSINESS.
        Para que las claves encriptadas sean las mismas en todos los Agentes de Sincronización Web.
        Despues que las claves hayan sido generadas si esto se llegara a cambiar no funcionaría la conexión SQL
        Al desplegar una nueva versión.
        */

        public static String mUserFTP = null;
        public static String mPassFTP = null;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {

            gPK = "" + Chr(55) + Chr(81) + Chr(76) + "_"
            + Chr(73) + Chr(55) + Chr(51) + Chr(51)
            + Chr(83) + Chr(78) + Chr(75) + Chr(52)
            + Chr(78) + Chr(50) + Chr(69) + Chr(74)
            + Chr(68) + Chr(49) + Chr(83) + "-"
            + Chr(66) + Chr(44) + Chr(66) + Chr(71)
            + Chr(52) + Chr(44) + Chr(90) + "_" + Chr(71);

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());

        }

        private static String Chr(int CharCode)
        {
            return Microsoft.VisualBasic.Strings.Chr(CharCode).ToString();
        }

    }
}
