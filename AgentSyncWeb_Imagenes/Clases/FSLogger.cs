﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace AgentSyncWebImagenes.Clases
{
    public class FSLogger
    {
        private string fileName;

        // si pasamos la ruta de un archivo, su utilizará ese para hacer el log
        public FSLogger(String file)
        {
            fileName = file;
        }

        // caso contrario se utiliza uno por defecto
        public FSLogger()
        {
            fileName = @"C:/log.txt";
        }

        public void EscribirLog(String logText)
        {
            try
            {
                using (StreamWriter w = File.AppendText(fileName))
                {
                    w.WriteLine(DateTime.Now.ToString() + " - " + logText);
                }
            }
            catch { }
        }

        public void EscribirLog(Exception ex, String PreMsg = null)
        {
            try
            {
                using (StreamWriter w = File.AppendText(fileName))
                {
                    
                    w.WriteLine("--------------------------------------------------------------------------------");
                    if (PreMsg != null) w.WriteLine("Log Message: " + PreMsg);

                    if (ex != null) { 

                        w.WriteLine(DateTime.Now.ToString() + " - Exception (" + ex.GetType().FullName + ")");
                        w.WriteLine("Message: " + ex.Message);
                        w.WriteLine("Source: " + ex.Source);
                        w.WriteLine("TargetSite: " + ex.TargetSite);
                        w.WriteLine("StackTrace: " + ex.StackTrace);
                        w.WriteLine("HResult: " + ex.HResult.ToString());
                        if (ex.InnerException != null) w.WriteLine("InnerException: " + ex.InnerException);
                        if (ex.HelpLink != null) w.WriteLine("HelpLink: " + ex.HelpLink);
                        if (ex.Data.Count > 0)
                        {
                            w.WriteLine("Additional Exception Data ------------------------------------------------------");
                            var Iterator = ex.Data.GetEnumerator();
                            while (Iterator.MoveNext())
                            {
                                w.WriteLine(Iterator.Entry.Key.ToString() + " : " + Iterator.Entry.Value.ToString());
                            }
                        }
                        
                    }

                    w.WriteLine("--------------------------------------------------------------------------------");
                    w.Close();

                }
            }
            catch { }
        }

    }
}
