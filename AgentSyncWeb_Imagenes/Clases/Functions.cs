﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgentSyncWebImagenes.Clases
{
    class Functions
    {
        public static string EncryptionKey;

        public Functions()
        {
            //LoadSettings();
        }

        static Functions()
        {
            EncryptionKey = Properties.Settings.Default.EncryptionKey;
        }

        public static String getAlternateTrustedConnectionString()
        {
            return
            "Server=" + Properties.Settings.Default.LocalDB_SQLServerName +
            ";Database=" + Properties.Settings.Default.LocalDB_SQLDBName +
            ";Trusted_Connection=True;"
            ;
        }

        public static SqlConnection getAlternateTrustedConnection(int ConnectionTimeout = -1)
        {
            return new SqlConnection(
            getAlternateTrustedConnectionString()
            + ((ConnectionTimeout != -1) ? "Connection Timeout = " + ConnectionTimeout + ";" : "")
            );
        }

        public static String getAlternateTrustedConnectionString(String ServerInstance, String DBName)
        {
            return
            "Server=" + ServerInstance +
            ";Database=" + DBName +
            ";Trusted_Connection=True;"
            ;
        }

        public static SqlConnection getAlternateTrustedConnection(String ServerInstance, String DBName, int ConnectionTimeout = -1)
        {
            return new SqlConnection(
            getAlternateTrustedConnectionString(ServerInstance, DBName)
            + ((ConnectionTimeout != -1) ? "Connection Timeout = " + ConnectionTimeout + ";" : "")
            );
        }

        public static String getAlternateConnectionString(String UserID, String Password)
        {
            return
            "Server=" + Properties.Settings.Default.LocalDB_SQLServerName +
            ";Database=" + Properties.Settings.Default.LocalDB_SQLDBName +
            ";User Id=" + UserID +
            ";Password=" + Password + ";"
            ;
        }

        public static SqlConnection getAlternateConnection(String UserID, String Password, int ConnectionTimeout = -1)
        {
            return new SqlConnection(
            getAlternateConnectionString(UserID, Password)
            + ((ConnectionTimeout != -1) ? "Connection Timeout = " + ConnectionTimeout + ";" : "")
            );
        }

        public static String getAlternateConnectionString(String ServerInstance, String DBName, String UserID, String Password, int ConnectionTimeout = -1)
        {
            return
            "Server=" + ServerInstance +
            ";Database=" + DBName +
            ";User Id=" + UserID +
            ";Password=" + Password + ";"
            ;
        }

        public static SqlConnection getAlternateConnection(String ServerInstance, String DBName, String UserID, String Password, int ConnectionTimeout = -1)
        {
            return new SqlConnection(
            getAlternateConnectionString(ServerInstance, DBName, UserID, Password)
            + ((ConnectionTimeout != -1) ? "Connection Timeout = " + ConnectionTimeout + ";" : "")
            );
        }

        public static Boolean CheckDBConnection(SqlConnection pDBConnection)
        {
            Boolean Returns = false;

            try
            {
                SqlCommand command = new SqlCommand("SELECT GETDATE() AS ServerTime", pDBConnection);
                pDBConnection.Open();

                SqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows)
                {
                    reader.Read();

                    Returns = true; // Convert.ToDateTime(reader["ServerTime"]);
                }

                reader.Close();
                return Returns;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
            finally
            {
                pDBConnection.Close();
            }
        }

        public static void QuickSyncDB(SqlConnection pDBConnection, String Query)
        {
            try
            {
                SqlCommand command = new SqlCommand(Query, pDBConnection);
                pDBConnection.Open();

                command.CommandTimeout = 30000;

                int rows = command.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                pDBConnection.Close();
            }
        }

        public DateTime getServerDatetime(SqlConnection DBConnection)
        {
            DateTime returns = DateTime.Now;

            try
            {
                SqlCommand command = new SqlCommand("SELECT GETDATE() AS ServerTime", DBConnection);
                DBConnection.Open();

                SqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows)
                {
                    reader.Read();

                    returns = Convert.ToDateTime(reader["ServerTime"]);
                }

                reader.Close();
                return returns;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return returns;
            }
            finally
            {
                DBConnection.Close();
            }
        }

        public static SqlCommand getParameterizedCommand(SqlParameter[] Params)
        {
            SqlCommand returnCommand = new SqlCommand();
            if (Params != null) returnCommand.Parameters.AddRange(Params);
            return returnCommand;
        }

        public static SqlCommand getParameterizedCommand(String CommandText, SqlParameter[] Params)
        {
            SqlCommand returnCommand = new SqlCommand(CommandText);
            if (Params != null) returnCommand.Parameters.AddRange(Params);
            return returnCommand;
        }

        public static SqlCommand getParameterizedCommand(String CommandText, SqlConnection Connection, SqlParameter[] Params)
        {
            SqlCommand returnCommand = new SqlCommand(CommandText, Connection);
            if (Params != null) returnCommand.Parameters.AddRange(Params);
            return returnCommand;
        }

        public static SqlCommand getParameterizedCommand(String CommandText, SqlConnection Connection, SqlTransaction Transaction, SqlParameter[] Params)
        {
            SqlCommand returnCommand = new SqlCommand(CommandText, Connection, Transaction);
            // For some reason the constructor doesn't work, so the Transaction must be explicitly applied to the Command.
            returnCommand.Transaction = Transaction;             
            if (Params != null) returnCommand.Parameters.AddRange(Params);
            return returnCommand;
        }

        public static string ByteToHex2(byte[] bin)
        {
            string Result = "";

            for (int i = 0; i < bin.Length; i++)
            {
                Result += bin[i].ToString("X2"); // Ex: byte [1] -> "01" instead of "1"
            }
            return (Result);
        }

        public static byte[] Hex2ToByte(String Text)
        {
            if (Text == null)
                return null;

            //if (Text.Length % 2 == 1)
            //    Text = '0' + Text; // Up to you whether to pad the first or last byte

            byte[] bin = new byte[Text.Length / 2];

            for (int i = 0; i < bin.Length; i++)
                bin[i] = Convert.ToByte(Text.Substring(i * 2, 2), 16);

            return bin;
        }

        public static void LoadSettings()
        {
            // Load All Settings using the LoadValue function which chains a SettingsProperty with
            // a Section and a Key Name in the final .ini style File. 
            try
            {

            if (!(System.IO.Directory.Exists(QuickSettings.FolderPath))) System.IO.Directory.CreateDirectory(QuickSettings.FolderPath);

            if (Properties.Settings.Default.PropertyValues.Count <= 0)
            {
                foreach (System.Configuration.SettingsProperty Property in Properties.Settings.Default.Properties.OfType<System.Configuration.SettingsProperty>())
                {
                    System.Configuration.SettingsPropertyValue Value = new System.Configuration.SettingsPropertyValue(Property);
                    Properties.Settings.Default.PropertyValues.Add(Value);
                }
            }

            Properties.Settings.Default.Properties["LocalDB_WindowsAuth"].LoadValue("LocalDB", "WindowsAuth");
            Properties.Settings.Default.Properties["LocalDB_SQLServerName"].LoadValue("LocalDB", "SQLServerName");
            Properties.Settings.Default.Properties["LocalDB_SQLDBName"].LoadValue("LocalDB", "SQLDBName");
            Properties.Settings.Default.Properties["LocalDB_SQLUser"].LoadValue("LocalDB", "SQLUser");
            Properties.Settings.Default.Properties["LocalDB_SQLPass"].LoadValue("LocalDB", "SQLPass");

            Properties.Settings.Default.Properties["RemoteDB_SQLServerName"].LoadValue("RemoteDB", "SQLServerName");
            Properties.Settings.Default.Properties["RemoteDB_SQLDBName"].LoadValue("RemoteDB", "SQLDBName");
            Properties.Settings.Default.Properties["RemoteDB_SQLUser"].LoadValue("RemoteDB", "SQLUser");
            Properties.Settings.Default.Properties["RemoteDB_SQLPass"].LoadValue("RemoteDB", "SQLPass");

            Properties.Settings.Default.Properties["Sucursal"].LoadValue("Agente", "Sucursal");
            Properties.Settings.Default.Properties["Deposito"].LoadValue("Agente", "Deposito");
            Properties.Settings.Default.Properties["PorcentajeExistencia"].LoadValue("Agente", "PorcentajeExistencia");

            Properties.Settings.Default.Properties["FTP_Auto"].LoadValue("FTP", "Auto");
            Properties.Settings.Default.Properties["FTP_AutoDB"].LoadValue("FTP", "AutoDB");
            Properties.Settings.Default.Properties["FTP_AutoInstance"].LoadValue("FTP", "AutoInstance");
            Properties.Settings.Default.Properties["FTP_Host"].LoadValue("FTP", "Host");
            Properties.Settings.Default.Properties["FTP_User"].LoadValue("FTP", "User");
            Properties.Settings.Default.Properties["FTP_Pwd"].LoadValue("FTP", "Pwd");
            Properties.Settings.Default.Properties["FTP_Port"].LoadValue("FTP", "Port");
            Properties.Settings.Default.Properties["FTP_Dir"].LoadValue("FTP", "Dir");
            Properties.Settings.Default.Properties["FTP_UploadContentBaseDir"].LoadValue("FTP", "UploadContentBaseDir");
            Properties.Settings.Default.Properties["FTP_ProcessURL"].LoadValue("FTP", "ProcessURL");
            Properties.Settings.Default.Properties["RutaArchivo"].LoadValue("FTP", "RutaArchivo");
            Properties.Settings.Default.Properties["RutaImgWeb"].LoadValue("FTP", "RutaImgWeb");

            // Loading values mark properties as "Changed", but we are essentially initializing them so we 
            // use this function to Remove "Changed" status for all settings.

            Properties.Settings.Default.Clean();

            // From now on, we access settings using the regular strongly-typed settings class which we define
            // using the Properties.Settings UI. Note that you should define all properties which won't
            // be ReadOnly as UserScoped. If you use some ApplicationScoped Settings, just can just use them
            // but you can't write any values to them, you would get a Runtime Exception by using LoadValue or a
            // Compiler Error if you intend to assign other values explicitly using the strongly-typed members.

            // So here is as we use them:

            // Properties.Settings.Default.Language = "en";

            // And here we will save all settings which were Changed.

            // Properties.Settings.Default.Save();

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

        }

        public static String Tab()
        {
            return "\t";
        }

        public static String Tab(int HowMany)
        {
            String Tabs = String.Empty;
            for (int i = 1; i <= HowMany; i++)
            {
                Tabs += Tab();
            }
            return Tabs;
        }

        public static String NewLine()
        {
            return System.Environment.NewLine;
        }

        public static String NewLine(int HowMany)
        {
            String Lines = String.Empty;
            for (int i = 1; i <= HowMany; i++)
            {
                Lines += System.Environment.NewLine;
            }
            return Lines;
        }

        public static System.Drawing.Color getColorOrEmpty(String ColorInput)
        {

            System.Drawing.Color MyColor = System.Drawing.Color.Empty; int ColorNumber;

            String[] ColorDetails;

            try { ColorDetails = ColorInput.Split(','); }
            catch (Exception) { return MyColor; }

            if (ColorDetails.Length == 4)
                try
                {
                    MyColor = System.Drawing.Color.FromArgb(Convert.ToInt32(ColorDetails[0]),
                    Convert.ToInt32(ColorDetails[1]), Convert.ToInt32(ColorDetails[2]), Convert.ToInt32(ColorDetails[3]));
                }
                catch (Exception) { MyColor = System.Drawing.Color.Empty; return MyColor; }

            if (ColorDetails.Length == 3)
                try
                {
                    MyColor = System.Drawing.Color.FromArgb(Convert.ToInt32(ColorDetails[0]),
                    Convert.ToInt32(ColorDetails[1]), Convert.ToInt32(ColorDetails[2]));
                }
                catch (Exception) { MyColor = System.Drawing.Color.Empty; return MyColor; }

            if (ColorDetails.Length == 1)
            {
                if (int.TryParse(ColorDetails[0], out ColorNumber))
                {
                    try { MyColor = System.Drawing.Color.FromArgb(ColorNumber); }
                    catch (Exception) { MyColor = System.Drawing.Color.Empty; return MyColor; }
                }
                else
                {
                    if (ColorDetails[0].Contains("#"))
                        try { MyColor = System.Drawing.ColorTranslator.FromHtml(ColorDetails[0]); }
                        catch (Exception) { MyColor = System.Drawing.Color.Empty; return MyColor; }
                    else
                        try { MyColor = System.Drawing.Color.FromName(ColorDetails[0]); }
                        catch (Exception) { MyColor = System.Drawing.Color.Empty; return MyColor; }
                }
            }

            return MyColor;
        }

        public static System.Drawing.Color getColorOrDefault(String ColorInput, System.Drawing.Color FallBackColor)
        {

            System.Drawing.Color MyColor = FallBackColor; int ColorNumber;

            String[] ColorDetails;

            try { ColorDetails = ColorInput.Split(','); }
            catch (Exception) { return MyColor; }

            if (ColorDetails.Length == 4)
                try
                {
                    MyColor = System.Drawing.Color.FromArgb(Convert.ToInt32(ColorDetails[0]),
                    Convert.ToInt32(ColorDetails[1]), Convert.ToInt32(ColorDetails[2]), Convert.ToInt32(ColorDetails[3]));
                }
                catch (Exception) { MyColor = FallBackColor; return MyColor; }

            if (ColorDetails.Length == 3)
                try
                {
                    MyColor = System.Drawing.Color.FromArgb(Convert.ToInt32(ColorDetails[0]),
                    Convert.ToInt32(ColorDetails[1]), Convert.ToInt32(ColorDetails[2]));
                }
                catch (Exception) { MyColor = FallBackColor; return MyColor; }

            if (ColorDetails.Length == 1)
            {
                if (int.TryParse(ColorDetails[0], out ColorNumber))
                {
                    try { MyColor = System.Drawing.Color.FromArgb(ColorNumber); }
                    catch (Exception) { MyColor = FallBackColor; return MyColor; }
                }
                else
                {
                    if (ColorDetails[0].Contains("#"))
                        try { MyColor = System.Drawing.ColorTranslator.FromHtml(ColorDetails[0]); }
                        catch (Exception) { MyColor = FallBackColor; return MyColor; }
                    else
                        try { MyColor = System.Drawing.Color.FromName(ColorDetails[0]); }
                        catch (Exception) { MyColor = FallBackColor; return MyColor; }
                }
            }

            return MyColor;
        }

        //public static long GetZipExtractedSize(String FolderPath)
        //{

        //    try
        //    {
        //        System.IO.Compression.ZipArchive ZipFile = System.IO.Compression.ZipFile.OpenRead(FolderPath);

        //        long ExtractedBytes = 0;                

        //        foreach (System.IO.Compression.ZipArchiveEntry Item in ZipFile.Entries)
        //        {
        //            //Console.WriteLine(Item.Length);
        //            ExtractedBytes += Item.Length;
        //        }

        //        return ExtractedBytes;
        //    }
        //    catch (Exception Any)
        //    {
        //        Console.WriteLine(Any.Message);
        //        return 0;
        //    }

        //}

        //public static long GetZipCompressedSize(String FolderPath)
        //{

        //    try
        //    {
        //        System.IO.Compression.ZipArchive ZipFile = System.IO.Compression.ZipFile.OpenRead(FolderPath);

        //        long CompressedBytes = 0;

        //        foreach (System.IO.Compression.ZipArchiveEntry Item in ZipFile.Entries)
        //        {
        //            //Console.WriteLine(Item.Length);
        //            CompressedBytes += Item.Length;
        //        }

        //        return CompressedBytes;
        //    }
        //    catch (Exception Any)
        //    {
        //        Console.WriteLine(Any.Message);
        //        return 0;
        //    }

        //}

        public static Int64 GetDirectorySize(String FolderPath)
        {

            try
            {
                String[] AllFilesWithinFolder = System.IO.Directory.GetFiles(FolderPath, "*", System.IO.SearchOption.AllDirectories);

                long TotalBytes = 0;

                foreach (String FilePath in AllFilesWithinFolder)
                {
                    System.IO.FileInfo FileData = new System.IO.FileInfo(FilePath);
                    TotalBytes += FileData.Length;
                }

                return TotalBytes;
            }
            catch (Exception Any)
            {
                Console.WriteLine(Any.Message);
                return 0;
            }

        }

        public static String PeopleFriendlyFormattedSize(long Bytes)
        {

            // This is for values between 1000 and 1023 so people won't have to read something like 1011 B or 0.98 KB, 
            // We know such Byte values are not exactly 1 KB, but the difference is minimal so we can treat it like that
            // since people commonly deal with values that go in the order of { 10, 100, 1000, 1000000 } and such.
            // Here we make the calculations so people don't have to read either too big or too small values.            

            // Bytes

            if (Bytes <= 0) return "0|B";

            if (Bytes > 1 && Bytes < 1000)
            {
                return Bytes.ToString() + "|" + "B";
            }
            else if (Bytes >= 1000 && Bytes <= 1024)
            {
                return "1|KB";
            }

            // KiloBytes

            double Result = ((double)Bytes / 1024);

            if (Result > 1 && Result < 1000)
            {
                return Math.Round(Result, 2).ToString() + "|" + "KB";
            }
            else if (Result >= 1000 && Result <= 1024)
            {
                return "1|MB";
            }

            // MegaBytes

            Result = (Result / 1024);

            if (Result > 1 && Result < 1000)
            {
                return Math.Round(Result, 2).ToString() + "|" + "MB";
            }
            else if (Result >= 1000 && Result <= 1024)
            {
                return "1|GB";
            }

            // GigaBytes

            Result = (Result / 1024);

            if (Result > 1 && Result < 1000)
            {
                return Math.Round(Result, 2).ToString() + "|" + "GB";
            }
            else if (Result >= 1000 && Result <= 1024)
            {
                return "1|TB";
            }

            // TeraBytes and Beyond...

            Result = (Result / 1024);

            if (Result > 1 && Result < 1000)
            {
                return Math.Round(Result, 2).ToString() + "|" + "MB";
            }

            return "0|B";

        }

        public static String RealFormattedSize(long Bytes)
        {

            if (Bytes <= 0) return "0|B";

            double Result = ((double)Bytes / 1024);

            // Bytes
            if (Result < 1)
            {
                return Bytes.ToString() + "|" + "B";
            }

            // KiloBytes
            if (Result > 1 && Result < 1024)
            {
                return Math.Round(Result, 2).ToString() + "|" + "KB";
            }

            // MegaBytes

            Result = (Result / 1024);

            if (Result >= 1 && Result < 1024)
            {
                return Math.Round(Result, 2).ToString() + "|" + "MB";
            }

            // GigaBytes

            Result = (Result / 1024);

            if (Result >= 1 && Result < 1024)
            {
                return Math.Round(Result, 2).ToString() + "|" + "GB";
            }

            // TeraBytes and Beyond...

            Result = (Result / 1024);

            if (Result >= 1 && Result < 1024)
            {
                return Math.Round(Result, 2).ToString() + "|" + "TB";
            }

            return "0|B";

        }

        public static String HttpPost(String URI, String Parameters)
        {

            try
            {
                System.Net.WebRequest req = System.Net.WebRequest.Create(URI);
                //Add these, as we're doing a POST
                req.ContentType = "application/x-www-form-urlencoded";
                req.Method = "POST";
                //We need to count how many bytes we're sending. 
                //Post'ed Faked Forms should be name=value&
                byte[] bytes = System.Text.Encoding.ASCII.GetBytes(Parameters);
                req.ContentLength = bytes.Length;
                System.IO.Stream os = req.GetRequestStream();
                os.Write(bytes, 0, bytes.Length); //Push it out there
                os.Close();

                System.Net.WebResponse resp = req.GetResponse();
                if (resp == null) return null;
                System.IO.StreamReader sr =
                      new System.IO.StreamReader(resp.GetResponseStream());
                return sr.ReadToEnd().Trim();

            }
            catch (System.Exception Any) {
                Program.Logger.EscribirLog(Any, "Ejecutando script de mantenimiento.");
                return null;
            }

        }

        public static dynamic SafeCreateObject(String pClass, String pServerName = "")
        {

            try
            {

                Type ObjType = null;

                if (pServerName.Trim().Length == 0)
                {
                    ObjType = Type.GetTypeFromProgID(pClass, true);
                }
                else
                {
                    ObjType = Type.GetTypeFromProgID(pClass, pServerName, true);
                }

                dynamic ObjInstance = Activator.CreateInstance(ObjType);

                return ObjInstance;

            }
            catch (Exception Any)
            {
                Program.Logger.EscribirLog(Any, "No se pudo instanciar el objeto [" + pClass + "]");
                return null;
            }

        }

        public static String FindAbsolutePathToRoot(System.IO.DirectoryInfo RootDir, System.IO.DirectoryInfo CurrentDir, String PathSeparator = "/")
        {

            String Path = String.Empty; Boolean ParentFound = false;

            ParentFound = String.Equals(CurrentDir.Parent.FullName, RootDir.Parent.FullName, StringComparison.Ordinal);

            while (!ParentFound)
            {

                Path = CurrentDir.Name + (Path.Length <= 0 ? String.Empty : PathSeparator) + Path;

                if (CurrentDir.Parent != null)
                {
                    CurrentDir = CurrentDir.Parent;
                        
                    ParentFound = String.Equals(CurrentDir.Parent.FullName, RootDir.Parent.FullName, StringComparison.Ordinal);
                }
                else { break; }
                
            }

            if (!ParentFound)
                return null;
            else
                return Path;

        }
            
    }
}
