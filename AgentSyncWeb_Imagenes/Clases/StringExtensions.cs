﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data.SqlClient;
using AgentSyncWebImagenes.Clases;

namespace AgentSyncWebImagenes.Clases
{
    public static class StringExtensions
    {
        /// <summary>
        /// Returns the last few characters of the string with a length
        /// specified by the given parameter. If the string's length is less than the 
        /// given length the complete string is returned. If length is zero or 
        /// less an empty string is returned
        /// </summary>
        /// <param name="s">the string to process</param>
        /// <param name="length">Number of characters to return</param>
        /// <returns></returns>
        public static string Right(this string s, int length)
        {
            length = Math.Max(length, 0);

            if (s.Length > length)
            {
                return s.Substring(s.Length - length, length);
            }
            else
            {
                return s;
            }
        }

        /// <summary>
        /// Returns the first few characters of the string with a length
        /// specified by the given parameter. If the string's length is less than the 
        /// given length the complete string is returned. If length is zero or 
        /// less an empty string is returned
        /// </summary>
        /// <param name="s">the string to process</param>
        /// <param name="length">Number of characters to return</param>
        /// <returns></returns>
        public static string Left(this string s, int length)
        {
            length = Math.Max(length, 0);

            if (s.Length > length)
            {
                return s.Substring(0, length);
            }
            else
            {
                return s;
            }
        }

        public static Boolean isUndefined(this String Text)
        {
            if (Text != null)
            {
                return ((Text.Equals(string.Empty)) || (Text.Equals("undefined", StringComparison.OrdinalIgnoreCase)));
            }
            else { return true; }
        }

        public static void LoadValue(this SettingsProperty Property, String Section, String Key)
        {
            try
            {
                Properties.Settings.Default.PropertyValues[Property.Name].PropertyValue =
                    Convert.ChangeType(QuickSettings.Get(Section, Key, Property), Property.PropertyType);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public static void LoadValue(this SettingsProperty Property)
        {
            try
            {
                Properties.Settings.Default.PropertyValues[Property.Name].PropertyValue =
                    Convert.ChangeType(QuickSettings.Get(Property), Property.PropertyType);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public static void KeepValue(this SettingsPropertyValue Item)
        {
            //QuickSettings.Set(
            //    Item.Property.Attributes["Section"].ToString(),
            //    Item.Property.Attributes["Key"].ToString(),
            //    Item.PropertyValue.ToString()
            //    );

            QuickSettings.Set(Item);

        }

        public static void Clean(this Properties.Settings Settings)
        {
            foreach (SettingsPropertyValue Item in Settings.PropertyValues)
            {
                Item.IsDirty = false; // Ya lave los coroto. / EN: Ya clean los corotes.
            } 
        }

        public static String SecureHash(this String Input)
        {
            try
            {
                System.Text.Encoding CharSet = System.Text.Encoding.Unicode;

                byte[] bin = CharSet.GetBytes(Input);

                byte[] Key = CharSet.GetBytes("isADS-SQL-Credentials-JustToBeSure");
                
                System.Security.Cryptography.HMACSHA512 Algorithym = new System.Security.Cryptography.HMACSHA512(Key);

                byte[] Return = Algorithym.ComputeHash(bin);

                String Result = Functions.ByteToHex2(Return);

                return Result;
            }
            catch (Exception)
            {
                return "undefined";
            }
        }

        public static String SecureHash(this String Input, String CustomKey)
        {
            try
            {
                System.Text.Encoding CharSet = System.Text.Encoding.Unicode;

                byte[] bin = CharSet.GetBytes(Input);

                byte[] Key = CharSet.GetBytes(CustomKey);

                System.Security.Cryptography.HMACSHA512 Algorithym = new System.Security.Cryptography.HMACSHA512(Key);

                byte[] Return = Algorithym.ComputeHash(bin);

                String Result = Functions.ByteToHex2(Return);

                return Result;
            }
            catch (Exception)
            {
                return "undefined";
            }
        }

        public static SqlConnection OpenGet(this SqlConnection Connection)
        {
            try { if (Connection.State == System.Data.ConnectionState.Closed) Connection.Open(); }
            catch (Exception e) { Console.WriteLine(e.Message); }
            return Connection;
        }

        public static Boolean IsNullOrEmpty(this Array Elements)
        {
            return ((Elements == null) ? (true) : (Elements.Length <= 0));
        }

        public static String DebugTestMsg(this String Text)
        {
            System.Windows.Forms.MessageBox.Show(Text);
            return Text;
        }

        public static Boolean In<T>(this T Item, params T[] Items)
        {
            if (Items == null)
                throw new ArgumentNullException("items");

            return Items.Contains(Item);
        }

    }
}
