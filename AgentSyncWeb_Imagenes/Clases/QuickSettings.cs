﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.Configuration;
using AgentSyncWebImagenes.Clases;

namespace AgentSyncWebImagenes.Clases
{
    /// <summary>
    /// Create a New INI file to store or load data
    /// </summary>
    public static class QuickSettings
    {
        public static String ApplicationName;
        public static String FilePath;
        public static String FolderPath;
        public static String FileName;
        private static Mode DocType = Mode.Ini;

        private enum Mode
        {
            Ini,
            Xml
        }

        private static String ProgramDataDir;

        static QuickSettings()
        {
            ApplicationName = Properties.Settings.Default.ApplicationName; // "isADS";
            FileName = "Stellar.Config";

            FolderPath = getDataFolder();
            FilePath = FolderPath + FileName;           
        }

        private static String getDataFolder()
        {

            if (ProgramDataDir == null)
            {
                switch (DocType)
                {
                    case Mode.Ini: ProgramDataDir = Properties.Settings.Default.ProgramDataDir + ApplicationName + "\\"; break;
                    case Mode.Xml: ProgramDataDir = Properties.Settings.Default.ProgramDataDir; break;
                }                 
            }

            try
            {
                String tmpDrive;

                tmpDrive =
                Assembly.GetExecutingAssembly().Location.Replace(
                    Assembly.GetExecutingAssembly().GetName().Name + ".exe", String.Empty
                );

                return tmpDrive;
            }
            catch (Exception tmpEx)
            {
                Console.WriteLine(tmpEx.Message);

                String tmpDrive =
                Assembly.GetExecutingAssembly().Location.Replace(
                    Assembly.GetExecutingAssembly().GetName().Name + ".exe", String.Empty
                );

                return tmpDrive;
            }
        }


        /// <summary>
        /// Write Data to the INI File
        /// </summary>
        /// <PARAM name="Section"></PARAM>
        /// Section name
        /// <PARAM name="Key"></PARAM>
        /// Key Name
        /// <PARAM name="Value"></PARAM>
        /// Value Name
        public static void Set(String Section, String Key, String Value)
        {
            switch (DocType)
            {
                case Mode.Ini:
                    {
                        new IniSettings().Set(Section, Key, Value);
                        break;
                    }
                case Mode.Xml:
                    {
                        new XmlSettings().Set(Section, Key, Value);
                        break;
                    }
            }
        }

        /// <summary>
        /// Write Data to the Ini/Xml File
        /// </summary>
        /// <PARAM name="Section"></PARAM>
        /// Section name
        /// <PARAM name="Key"></PARAM>
        /// Key Name
        /// <PARAM name="Value"></PARAM>
        /// Value Item
        public static void Set(SettingsPropertyValue Item)
        {
            switch (DocType)
            {
                case Mode.Ini:
                    {
                        new IniSettings().Set(Item);
                        break;
                    }
                case Mode.Xml:
                    {
                        new XmlSettings().Set(Item);
                        break;
                    }
            }
        }

        /// <summary>
        /// Read Data
        /// </summary>
        /// <PARAM name="Section"></PARAM>
        /// <PARAM name="Key"></PARAM>
        /// <PARAM name="CustomDefaultValue"></PARAM>
        /// <returns></returns>
        public static Object Get(String Section, String Key, String CustomDefaultValue)
        {
            switch (DocType)
            {
                case Mode.Ini:
                    {
                        return new IniSettings().Get(Section, Key, CustomDefaultValue);
                    }
                case Mode.Xml:
                    {
                        return new XmlSettings().Get(Section, Key, CustomDefaultValue);
                    }
                default:
                    return null;
            }
        }

        /// <summary>
        /// Read Data
        /// </summary>
        /// <PARAM name="Section"></PARAM>
        /// <PARAM name="Key"></PARAM>
        /// <PARAM name="CustomDefaultValue"></PARAM>
        /// <returns></returns>
        public static Object Get(String Section, String Key, SettingsProperty Property)
        {
            switch (DocType)
            {
                case Mode.Ini:
                    {
                        return new IniSettings().Get(Section, Key, Property);
                    }
                case Mode.Xml:
                    {
                        return new XmlSettings().Get(Section, Key, Property);
                    }
                default:
                    return null;
            }
        }

        public static Object Get(SettingsProperty Property)
        {
            switch (DocType)
            {
                case Mode.Ini:
                    {
                        return new IniSettings().Get(Property);
                    }
                case Mode.Xml:
                    {
                        return new XmlSettings().Get(Property);
                    }
                default:
                    return null;
            }
        }
    }
}
