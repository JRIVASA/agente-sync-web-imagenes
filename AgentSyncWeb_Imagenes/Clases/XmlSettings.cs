﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.Configuration;
using System.Xml.Linq;
using AgentSyncWebImagenes.Clases;

namespace AgentSyncWebImagenes.Clases
{
    class XmlSettings
    {

        private readonly ValueReadability VRMode = ValueReadability.Simple;
        private readonly String WordToIdentifyElements = "Type"; // Hierarchy; Type; String.Empty;
        enum ValueReadability
        {
            Simple,
            ValueAsElement,
            ValueWithType
        }

        private XElement GetNewValue(String Key, String Value, String TypeFullName)
        {
            switch (VRMode)
            {
                case ValueReadability.Simple:
                    {
                        if (WordToIdentifyElements.isUndefined()) return new XElement(Key, Value);
                        else return new XElement(Key, new XAttribute(WordToIdentifyElements, "Value"), Value);
                    }
                case ValueReadability.ValueAsElement:
                    {
                        if (WordToIdentifyElements.isUndefined()) return new XElement(Key, new XElement("Value", Value));
                        else return new XElement(Key, new XAttribute(WordToIdentifyElements, "Value"), new XElement("Value", Value));
                    }
                case ValueReadability.ValueWithType:
                    {
                        if (WordToIdentifyElements.isUndefined()) return new XElement(Key, new XElement("Value", Value), new XElement("Type", TypeFullName));
                        else return new XElement(Key, new XAttribute(WordToIdentifyElements, "Value"), new XElement("Value", Value), new XElement("Type", TypeFullName));
                    }
                default:
                    {
                        return null;
                    }
            }
        }

        /// <summary>
        /// Write Data to the XML File
        /// </summary>
        /// <PARAM name="Section"></PARAM>
        /// Section name
        /// <PARAM name="Key"></PARAM>
        /// Key Name
        /// <PARAM name="Value"></PARAM>
        /// Value Name
        public void Set(String Section, String Key, String Value)
        {
            XDocument Config;
            
            try 
	        {
                Config = XDocument.Load(QuickSettings.FilePath, LoadOptions.None);
	        }
	        catch (System.IO.FileNotFoundException)
	        {
                Config = new XDocument(new XElement("Stellar", String.Empty));
                //if (!WordToIdentifyElements.isUndefined()) Config.Root.SetAttributeValue(WordToIdentifyElements, "Root");
            }            

            XElement AppSettingsNode = Config.Root.Element(QuickSettings.ApplicationName);
            if (AppSettingsNode == null) 
            {
                AppSettingsNode = new XElement(QuickSettings.ApplicationName, String.Empty);
                Config.Root.Add(AppSettingsNode);
                if (!WordToIdentifyElements.isUndefined()) AppSettingsNode.SetAttributeValue(WordToIdentifyElements, "App");
            }
            
            XElement SectionNode = AppSettingsNode.Element(Section);
            if (SectionNode == null) 
            {
                SectionNode = new XElement(Section, String.Empty);
                AppSettingsNode.Add(SectionNode);
                if (!WordToIdentifyElements.isUndefined()) SectionNode.SetAttributeValue(WordToIdentifyElements, "Section");
            }

            XElement KeyNode = SectionNode.Element(Key);
            if (KeyNode == null)
            {
                KeyNode = GetNewValue(Key, Value, typeof(String).FullName);
                SectionNode.Add(KeyNode);
            }
            else
            {
                KeyNode.ReplaceWith(GetNewValue(Key, Value, typeof(String).FullName));
            }

            Config.Save(QuickSettings.FilePath, SaveOptions.None);
        }

        /// <summary>
        /// Write Data to the INI File
        /// </summary>
        /// <PARAM name="Section"></PARAM>
        /// Section name
        /// <PARAM name="Key"></PARAM>
        /// Key Name
        /// <PARAM name="Value"></PARAM>
        /// Value Item
        public void Set(SettingsPropertyValue Item)
        {
            XDocument Config;

            String Section = Item.Property.Attributes["Section"].ToString();
            String Key = Item.Property.Attributes["Key"].ToString();

            try
            {
                Config = XDocument.Load(QuickSettings.FilePath, LoadOptions.None);
            }
            catch (System.IO.FileNotFoundException)
            {
                Config = new XDocument(new XElement("Stellar", String.Empty));
                //if (!WordToIdentifyElements.isUndefined()) Config.Root.SetAttributeValue(WordToIdentifyElements, "Root");
            }

            XElement AppSettingsNode = Config.Root.Element(QuickSettings.ApplicationName);
            if (AppSettingsNode == null)
            {
                AppSettingsNode = new XElement(QuickSettings.ApplicationName, String.Empty);
                Config.Root.Add(AppSettingsNode);
                if (!WordToIdentifyElements.isUndefined()) AppSettingsNode.SetAttributeValue(WordToIdentifyElements, "App");
            }

            XElement SectionNode = AppSettingsNode.Element(Section);
            if (SectionNode == null)
            {
                SectionNode = new XElement(Section, String.Empty);
                AppSettingsNode.Add(SectionNode);
                if (!WordToIdentifyElements.isUndefined()) SectionNode.SetAttributeValue(WordToIdentifyElements, "Section");
            }

            XElement KeyNode = SectionNode.Element(Key);
            if (KeyNode == null)
            {
                KeyNode = GetNewValue(Key, Item.PropertyValue.ToString(), Item.Property.PropertyType.FullName);
                SectionNode.Add(KeyNode);
            }
            else
            {
                KeyNode.ReplaceWith(GetNewValue(Key, Item.PropertyValue.ToString(), Item.Property.PropertyType.FullName));
            }

            Config.Save(QuickSettings.FilePath, SaveOptions.None);
        }

        /// <summary>
        /// Read Data Value From the Xml File
        /// </summary>
        /// <PARAM name="Section"></PARAM>
        /// <PARAM name="Key"></PARAM>
        /// <PARAM name="CustomDefaultValue"></PARAM>
        /// <returns></returns>
        public String Get(String Section, String Key, String CustomDefaultValue)
        {
            XDocument Config; String Value;

            try
            {
                Config = XDocument.Load(QuickSettings.FilePath, LoadOptions.None);

                Value = GetValue(Config.
                    Root
                        .Element(QuickSettings.ApplicationName)
                            .Element(Section)
                                .Element(Key)
                                ).ToString();
            }
            catch (System.IO.FileNotFoundException)
            {
                // Normal.
                Value = CustomDefaultValue;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message.ToString()); // Read issue? bad document?.
                Value = CustomDefaultValue;
            }

            return Value;
        }

        private Object GetValue(XElement Element)
        {
            switch (VRMode)
            {
                case ValueReadability.Simple:
                    {
                        return Element.Value;
                    }
                case ValueReadability.ValueAsElement: case ValueReadability.ValueWithType:
                    {
                        return Element.Element("Value").Value;
                    }
                default:
                    return null;
            }
        }

        /// <summary>
        /// Read Data Value From the Xml File
        /// </summary>
        /// <PARAM name="Section"></PARAM>
        /// <PARAM name="Key"></PARAM>
        /// <PARAM name="Property"></PARAM>
        /// <returns></returns>
        public Object Get(String Section, String Key, SettingsProperty Property)
        {
            XDocument Config; Object Value;

            try
            {
                Config = XDocument.Load(QuickSettings.FilePath, LoadOptions.None);

                Value = GetValue(Config.
                    Root
                        .Element(QuickSettings.ApplicationName)
                            .Element(Section)
                                .Element(Key)
                                );
            }
            catch (System.IO.FileNotFoundException)
            {             
                // Normal.
                Value = Property.DefaultValue;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message.ToString()); // Read issue? bad document?.
                Value = Property.DefaultValue;
            }

            if (Property.Attributes["Section"] == null) Property.Attributes.Add("Section", Section);
            if (Property.Attributes["Key"] == null) Property.Attributes.Add("Key", Key);

            return Value;
        }

        public Object Get(SettingsProperty Property)
        {
            XDocument Config; Object Value;

            try
            {
                Config = XDocument.Load(QuickSettings.FilePath, LoadOptions.None);

                Value = GetValue(Config.
                    Root
                        .Element(QuickSettings.ApplicationName)
                            .Element(Property.Attributes["Section"].ToString())
                                .Element(Property.Attributes["Key"].ToString())
                                );
            }
            catch (System.IO.FileNotFoundException)
            {
                // Normal.
                Value = Property.DefaultValue;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message.ToString()); // Read issue? bad document?.
                Value = Property.DefaultValue;
            }

            return Value;
        }

    }
}
