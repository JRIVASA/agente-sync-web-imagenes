﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.Configuration;

namespace AgentSyncWebImagenes.Clases
{
    class IniSettings
    {
        [DllImport("kernel32")]
        private static extern long WritePrivateProfileString(string Section,
            string Key, string Value, string filePath);
        [DllImport("kernel32")]
        private static extern int GetPrivateProfileString(string Section,
                 string Key, string def, StringBuilder retVal,
            int size, string filePath);

        /// <summary>
        /// Write Data to the INI File
        /// </summary>
        /// <PARAM name="Section"></PARAM>
        /// Section name
        /// <PARAM name="Key"></PARAM>
        /// Key Name
        /// <PARAM name="Value"></PARAM>
        /// Value Name
        public void Set(String Section, String Key, String Value)
        {
            WritePrivateProfileString(Section, Key, Value, QuickSettings.FilePath);
        }

        /// <summary>
        /// Write Data to the INI File
        /// </summary>
        /// <PARAM name="Section"></PARAM>
        /// Section name
        /// <PARAM name="Key"></PARAM>
        /// Key Name
        /// <PARAM name="Value"></PARAM>
        /// Value Item
        public void Set(SettingsPropertyValue Item)
        {
            WritePrivateProfileString(
                Item.Property.Attributes["Section"].ToString(),
                Item.Property.Attributes["Key"].ToString(),
                Item.PropertyValue.ToString(),
                QuickSettings.FilePath);
        }

        /// <summary>
        /// Read Data Value From the Ini File
        /// </summary>
        /// <PARAM name="Section"></PARAM>
        /// <PARAM name="Key"></PARAM>
        /// <PARAM name="Item"></PARAM>
        /// <returns></returns>
        public String Get(String Section, String Key, String CustomDefaultValue)
        {
            StringBuilder Temp = new StringBuilder(10000);
            int i = GetPrivateProfileString(Section, Key, CustomDefaultValue, Temp,
                                            10000, QuickSettings.FilePath);
            return Temp.ToString();
        }

        /// <summary>
        /// Read Data Value From the Ini File
        /// </summary>
        /// <PARAM name="Section"></PARAM>
        /// <PARAM name="Key"></PARAM>
        /// <PARAM name="CustomDefaultValue"></PARAM>
        /// <returns></returns>
        public String Get(String Section, String Key, SettingsProperty Property)
        {
            StringBuilder Temp = new StringBuilder(10000);

            int i = GetPrivateProfileString(Section, Key, Property.DefaultValue.ToString(), Temp,
                                            10000, QuickSettings.FilePath);

            if (Property.Attributes["Section"] == null) Property.Attributes.Add("Section", Section);
            if (Property.Attributes["Key"] == null) Property.Attributes.Add("Key", Key);

            return Temp.ToString();
        }

        public String Get(SettingsProperty Property)
        {
            StringBuilder Temp = new StringBuilder(10000);

            int i = GetPrivateProfileString(Property.Attributes["Section"].ToString(), Property.Attributes["Key"].ToString(), Property.DefaultValue.ToString(), Temp,
                                            10000, QuickSettings.FilePath);

            return Temp.ToString();
        }
    }
}
