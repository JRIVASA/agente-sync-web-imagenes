﻿using AgentSyncWebImagenes.Clases;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;

namespace AgentSyncWebImagenes.Database_Sync_Classes
{
    class DBSync
    {   
        /*public enum UploadDirDeletePolicy
        {
            None = -1,
            Files = 0,
            RootDir = 1
        }*/

        public static Boolean CheckFTPAccess(String HostName, ref String User, ref String Pass, String RemoteFilePath, String Port = "")
        {
            try
            {

                ftp Access = new ftp(HostName, User, Pass);

                Boolean Success = false;

                // Test connection success
                Success = Access.CheckFTPAccess("/");

                User = Access.ReturnFTPUser();
                Pass = Access.ReturnFTPPwd();

                Access = null;

                return Success;
            }
            catch (System.Exception Any)
            {
                Program.Logger.EscribirLog(Any, "Verificando Conexión al Servidor Remoto (FTP) [" + HostName + "]");
                return false;
            };

        }

        public static Boolean UploadFile(String HostName, String User, String Pass, String LocalFilePath, String RemoteFilePath, String Port = "")
        {
            try
            {
                ftp Access = new ftp(HostName, User, Pass);

                // Test connection success
                object TMP = Access.directoryListSimple("/");

                Access.delete (RemoteFilePath);
                Access.upload(RemoteFilePath, LocalFilePath);

                Access = null;

                return true;
            }
            catch (System.Exception e) { 
                Console.WriteLine(e.Message);
                return false;
            };
        }

        public static void DeleteImageWithoutInfo(String HostName, String User, String Pass, String LocalDir, String RemoteDir, String Port = "")
        {

            // Data structure to hold names of subfolders to be
            // examined for files.

            Stack<String> dirs = new Stack<String>();

            System.IO.DirectoryInfo LocalDirInfo = null; Int64 FailedCount = 0;

            try
            {
                LocalDirInfo = new System.IO.DirectoryInfo(LocalDir);
                if (!LocalDirInfo.Exists) LocalDirInfo = null;
            }
            catch { }

            if (LocalDirInfo == null)
            {
                Program.Logger.EscribirLog("El directorio [" + LocalDir + "] no existe o no es accesible. Verifique la configuración e intentelo de nuevo.");
                return;
            }

            dirs.Push(LocalDir);

            while (dirs.Count > 0)
            {
                string currentDir = dirs.Pop();
                string[] subDirs;
                try
                {
                    subDirs = System.IO.Directory.GetDirectories(currentDir);
                }
                // An UnauthorizedAccessException exception will be thrown if we do not have
                // discovery permission on a folder or file. It may or may not be acceptable 
                // to ignore the exception and continue enumerating the remaining files and 
                // folders. It is also possible (but unlikely) that a DirectoryNotFound exception 
                // will be raised. This will happen if currentDir has been deleted by
                // another application or thread after our call to Directory.Exists. The 
                // choice of which exceptions to catch depends entirely on the specific task 
                // you are intending to perform and also on how much you know with certainty 
                // about the systems on which this code will run.
                catch (System.Exception Any)
                {
                    Program.Logger.EscribirLog(Any, "Comprobando el directorio [" + currentDir + "]. Información adicional: ");
                    continue;
                }

                string[] files = null;
                try
                {
                    files = System.IO.Directory.GetFiles(currentDir);
                }
                catch (System.Exception Any)
                {
                    Program.Logger.EscribirLog(Any, "Buscando archivos en el directorio [" + currentDir + "]. Información adicional: ");
                    continue;
                }

                // Perform the required action on each file here.
                // Modify this block to perform your required task.

                System.IO.DirectoryInfo TmpDirInfo = null; System.IO.FileInfo TmpFileInfo = null; String TmpSubDir = null;

                try
                {
                    TmpDirInfo = new System.IO.DirectoryInfo(currentDir);
                    TmpSubDir = Functions.FindAbsolutePathToRoot(LocalDirInfo, TmpDirInfo);
                }
                catch (System.Exception Any)
                {
                    Program.Logger.EscribirLog(Any, "Comprobando el directorio [" + currentDir + "]. Información adicional: ");
                    continue;
                }

                foreach (string file in files)
                {

                    try
                    {
                        TmpFileInfo = new System.IO.FileInfo(file);
                    }
                    catch (System.Exception Any)
                    {
                        Program.Logger.EscribirLog(Any, "Comprobando el archivo [" + file + "]. Información adicional: ");
                        continue;
                    }

                    if (TmpFileInfo.Name.Equals("Img.jpg", StringComparison.OrdinalIgnoreCase))
                    {
                        System.IO.FileInfo TxtInfo = new System.IO.FileInfo( TmpFileInfo.Directory + @"\Info.txt");
                        if (!(TxtInfo.Exists)) {
                            try { TmpFileInfo.Delete(); }
                            catch (Exception Any) { Program.Logger.EscribirLog(Any, "El archivo [" + TmpFileInfo.FullName + "] no se pudo eliminar. Información adicional: "); }
                        }
                    }

                    if (TmpFileInfo.Name.Equals("Img1.jpg", StringComparison.OrdinalIgnoreCase))
                    {
                        System.IO.FileInfo TxtInfo = new System.IO.FileInfo( TmpFileInfo.Directory + @"\Info1.txt");
                        if (!(TxtInfo.Exists))
                        {
                            try { TmpFileInfo.Delete(); }
                            catch (Exception Any) { Program.Logger.EscribirLog(Any, "El archivo [" + TmpFileInfo.FullName + "] no se pudo eliminar. Información adicional: "); }
                        }
                    }

                    if (TmpFileInfo.Name.Equals("Img2.jpg", StringComparison.OrdinalIgnoreCase))
                    {
                        System.IO.FileInfo TxtInfo = new System.IO.FileInfo( TmpFileInfo.Directory + @"\Info2.txt");
                        if (!(TxtInfo.Exists))
                        {
                            try { TmpFileInfo.Delete(); }
                            catch (Exception Any) { Program.Logger.EscribirLog(Any, "El archivo [" + TmpFileInfo.FullName + "] no se pudo eliminar. Información adicional: "); }
                        }
                    }

                    if (TmpFileInfo.Name.Equals("Img3.jpg", StringComparison.OrdinalIgnoreCase))
                    {
                        System.IO.FileInfo TxtInfo = new System.IO.FileInfo( TmpFileInfo.Directory + @"\Info3.txt");
                        if (!(TxtInfo.Exists))
                        {
                            try { TmpFileInfo.Delete(); }
                            catch (Exception Any) { Program.Logger.EscribirLog(Any, "El archivo [" + TmpFileInfo.FullName + "] no se pudo eliminar. Información adicional: "); }
                        }
                    }

                    if (TmpFileInfo.Name.Equals("Img4.jpg", StringComparison.OrdinalIgnoreCase))
                    {
                        System.IO.FileInfo TxtInfo = new System.IO.FileInfo( TmpFileInfo.Directory + @"\Info4.txt");
                        if (!(TxtInfo.Exists))
                        {
                            try { TmpFileInfo.Delete(); }
                            catch (Exception Any) { Program.Logger.EscribirLog(Any, "El archivo [" + TmpFileInfo.FullName + "] no se pudo eliminar. Información adicional: "); }
                        }
                    }

                }

                // Push the subdirectories onto the stack for traversal.
                // This could also be done before handing the files.
                if (subDirs.Length > 0)
                    foreach (string str in subDirs)
                        dirs.Push(str);
                else if (TmpSubDir != String.Empty)
                    if (Functions.GetDirectorySize(currentDir) <= 0) try { TmpDirInfo.Delete(); }
                        catch (Exception Any) { Program.Logger.EscribirLog(Any, "El directorio [" + currentDir + "] no se pudo eliminar. Información adicional: "); }

            }

            Console.WriteLine(FailedCount);
            if (FailedCount > 0) Program.Logger.EscribirLog(null, "Cantidad de archivos Fallidos: " + FailedCount.ToString());

        }

        public static void UploadDir(String HostName, String User, String Pass, String LocalDir, String RemoteDir, String Port = "") //, UploadDirDeletePolicy DeletePolicy = UploadDirDeletePolicy.None)
        {

            ftp Access = new ftp(HostName, User, Pass);

            // Data structure to hold names of subfolders to be
            // examined for files.

            Stack<String> dirs = new Stack<String>();

            System.IO.DirectoryInfo LocalDirInfo = null; Int64 FailedCount = 0; Boolean FileOperationFailed = false;

            try { 
                LocalDirInfo = new System.IO.DirectoryInfo(LocalDir);
                if (!LocalDirInfo.Exists) LocalDirInfo = null;
            }
            catch { }

            if (LocalDirInfo == null)
            {
                Program.Logger.EscribirLog("El directorio [" + LocalDir + "] no existe o no es accesible. Verifique la configuración e intentelo de nuevo.");
                return;
            }
            
            dirs.Push(LocalDir);

            while (dirs.Count > 0)
            {
                string currentDir = dirs.Pop();
                string[] subDirs;
                try
                {
                    subDirs = System.IO.Directory.GetDirectories(currentDir);
                }
                // An UnauthorizedAccessException exception will be thrown if we do not have
                // discovery permission on a folder or file. It may or may not be acceptable 
                // to ignore the exception and continue enumerating the remaining files and 
                // folders. It is also possible (but unlikely) that a DirectoryNotFound exception 
                // will be raised. This will happen if currentDir has been deleted by
                // another application or thread after our call to Directory.Exists. The 
                // choice of which exceptions to catch depends entirely on the specific task 
                // you are intending to perform and also on how much you know with certainty 
                // about the systems on which this code will run.
                catch (System.Exception Any)
                {
                    Program.Logger.EscribirLog(Any, "Comprobando el directorio [" + currentDir + "]. Información adicional: ");
                    continue;
                }

                string[] files = null;
                try
                {
                    files = System.IO.Directory.GetFiles(currentDir);
                }
                catch (System.Exception Any)
                {
                    Program.Logger.EscribirLog(Any, "Buscando archivos en el directorio [" + currentDir + "]. Información adicional: ");
                    continue;
                }

                // Perform the required action on each file here.
                // Modify this block to perform your required task.

                System.IO.DirectoryInfo TmpDirInfo = null; System.IO.FileInfo TmpFileInfo = null; String TmpSubDir = null;

                try
                {
                    TmpDirInfo = new System.IO.DirectoryInfo(currentDir);
                    TmpSubDir = Functions.FindAbsolutePathToRoot(LocalDirInfo, TmpDirInfo);
                }
                catch (System.Exception Any)
                {
                    Program.Logger.EscribirLog(Any, "Comprobando el directorio [" + currentDir + "]. Información adicional: ");
                    continue;
                }

                if (TmpSubDir != String.Empty) // if TmpSubDir == String.Empty then it is the Root Dir, so we prevent actions on that directory.
                    Access.createDirectory(RemoteDir + TmpSubDir);
                
                foreach (string file in files)
                {
                    // Perform whatever action is required in your scenario.
                    //System.IO.FileInfo fi = new System.IO.FileInfo(file);
                    //Console.WriteLine("{0}: {1}, {2}", fi.Name, fi.Length, fi.CreationTime);

                    //List<String> AllowedExtensions = new List<String>(){".jpg", ".png", ".ico", ".bmp", ".svg", ".tif", ".txt", ".xml", ".json", ".rtf"};

                    try
                    {
                        TmpFileInfo = new System.IO.FileInfo(file);
                    }
                    catch (System.Exception Any)
                    {
                        Program.Logger.EscribirLog(Any, "Comprobando el archivo [" + file + "]. Información adicional: ");
                        continue;
                    }

                    //if (AllowedExtensions.Contains(TmpFileInfo.Extension.ToLower()))
                    if (TmpFileInfo.Extension.ToLower().In(
                    ".jpg", ".png", ".ico", ".bmp", ".svg", ".tif", ".txt", ".xml", ".json", ".rtf"))
                    {
                        Access.delete(RemoteDir + TmpSubDir + "/" + TmpFileInfo.Name);
                        Access.upload(RemoteDir + TmpSubDir + "/" + TmpFileInfo.Name, file);
                        FileOperationFailed = (Access.getFileSizeinBytes(RemoteDir + TmpSubDir + "/" + TmpFileInfo.Name) != TmpFileInfo.Length);
                        if (FileOperationFailed){
                            Program.Logger.EscribirLog(null, "El archivo [" + file + "] no se pudo subir al directorio web [" + RemoteDir + TmpSubDir + "/" + TmpFileInfo.Name + "]");
                            FailedCount ++;
                        } else { 
                            //if (DeletePolicy == UploadDirDeletePolicy.Files)
                            //{
                                try
                                {
                                    if (TmpFileInfo.Extension.Equals(".jpg", StringComparison.OrdinalIgnoreCase))
                                    {
                                        Access.delete(RemoteDir + TmpSubDir + "/" + "ThumbInfo.txt");
                                    }
                                }
                                catch (Exception Any)
                                {
                                    Console.WriteLine(Any);
                                }

                                try { TmpFileInfo.Delete(); }
                                catch (Exception Any) { Program.Logger.EscribirLog(Any, "El archivo [" + file + "] no se pudo eliminar. Información adicional: "); }
                            //}
                        }
                    }
                }

                // Push the subdirectories onto the stack for traversal.
                // This could also be done before handing the files.
                if (subDirs.Length > 0)
                    foreach (string str in subDirs)
                        dirs.Push(str);
                else if (TmpSubDir != String.Empty)
                    if (Functions.GetDirectorySize(currentDir) <= 0) try { TmpDirInfo.Delete(); }
                        catch (Exception Any) { Program.Logger.EscribirLog(Any, "El directorio [" + currentDir + "] no se pudo eliminar. Información adicional: "); }
            }

            Console.WriteLine(FailedCount);
            if (FailedCount > 0) Program.Logger.EscribirLog(null, "Cantidad de archivos Fallidos: " + FailedCount.ToString());

        }

        private static void RecuperacionInicialDeCredenciales()
        {

            dynamic mClsTmp = null;

            Program.mUserFTP = Properties.Settings.Default.FTP_User;
            Program.mPassFTP = Properties.Settings.Default.FTP_Pwd;

            //if (!(String.Equals(Program.mUserFTP, "SA", StringComparison.OrdinalIgnoreCase) && Program.mPassFTP.Length == 0))
            //{
            mClsTmp = Functions.SafeCreateObject("SQLSafeGuard.Service");

            if (mClsTmp != null)
            {
                Program.mUserFTP = mClsTmp.Decode(Program.gCodProducto, Program.gNombreProducto, Program.gPK, Program.mUserFTP);
                Program.mPassFTP = mClsTmp.Decode(Program.gCodProducto, Program.gNombreProducto, Program.gPK, Program.mPassFTP);
            }
            else
            {
                Program.Logger.EscribirLog("Error de autenticación. Debe instalar y registrar el componente DLL SQLSafeGuard.");
                System.Environment.Exit(0);
            }

        }

        public static void UploadData()
        {
            try
            {

                RecuperacionInicialDeCredenciales();

                String TmpHost; String TmpUser; String TmpPwd; String TmpDir; String TmpPort;
                GetFTPDetails(out TmpHost, out TmpUser, out TmpPwd, out TmpDir, out TmpPort);

                String TmpUrl = Properties.Settings.Default.FTP_ProcessURL;
                String TmpResp = Functions.HttpPost(TmpUrl, "");

                //System.Windows.Forms.MessageBox.Show("Borrando imágenes que no tienen info."); // Habilitar con Debug Mode

                DeleteImageWithoutInfo(TmpHost, TmpUser, TmpPwd, Properties.Settings.Default.FTP_UploadContentBaseDir + "\\Productos\\", Properties.Settings.Default.RutaArchivo); // "httpdocs/DB-IMG-PRODUCT/"

                //System.Windows.Forms.MessageBox.Show("Empezando a subir imágenes"); // Habilitar con Debug Mode

                UploadDir(TmpHost, TmpUser, TmpPwd, Properties.Settings.Default.FTP_UploadContentBaseDir + "\\Productos\\", Properties.Settings.Default.RutaArchivo); // "httpdocs/DB-IMG-PRODUCT/"
                UploadDir(TmpHost, TmpUser, TmpPwd, Properties.Settings.Default.FTP_UploadContentBaseDir + "\\SeccionesWeb\\", Properties.Settings.Default.RutaImgWeb); // "httpdocs/DB-IMG/");

                //System.Windows.Forms.MessageBox.Show("Proceso Finalizado"); // Habilitar con Debug Mode

            }
            catch (System.Exception Any)
            {
                Program.Logger.EscribirLog(Any, "Error al ejecutar el script de mantenimiento de recursos gráficos.");
            }

        }

        private static void GetFTPDetails(out String HostName, out String User, out String Pwd, out String DestDir, out String Port)
        {

            HostName = "";
            User = "";
            Pwd = "";
            DestDir = "";
            Port = "";

            try
            {

                if (Properties.Settings.Default.FTP_Auto)
                {
                    SqlConnection mCnLocal = Functions.getAlternateTrustedConnection(Properties.Settings.Default.FTP_AutoInstance, Properties.Settings.Default.FTP_AutoDB, 30);
                    mCnLocal.Open();

                    SqlCommand mSql = null;

                    mSql = Functions.getParameterizedCommand("SELECT * FROM TmpSettings", mCnLocal, null);

                    SqlDataReader mRs = mSql.ExecuteReader();

                    while (mRs.Read())
                    {

                        switch (mRs["Field"].ToString())
                        {
                            case "FTP_Host":
                                HostName = mRs["Value"].ToString();
                                break;
                            case "FTP_User":
                                User = mRs["Value"].ToString();
                                break;
                            case "FTP_Pwd":
                                Pwd = mRs["Value"].ToString();
                                break;
                            case "FTP_Dir":
                                DestDir = mRs["Value"].ToString();
                                break;
                            case "FTP_Port":
                                Port = mRs["Value"].ToString();
                                break;
                        }

                    }

                    mRs.Close();
                    mCnLocal.Close();

                }
                else
                {
                    
                    
                    HostName = Properties.Settings.Default.FTP_Host;
                    User = Properties.Settings.Default.FTP_User;
                    User = Program.mUserFTP;
                    Pwd = Properties.Settings.Default.FTP_Pwd;
                    Pwd = Program.mPassFTP;
                    DestDir = Properties.Settings.Default.FTP_Dir;
                    Port = Properties.Settings.Default.FTP_Port;
                }
            }
            catch (System.Exception e) {
                Console.WriteLine(e.Message);
            }

            if (!CheckFTPAccess(HostName, ref User, ref Pwd, DestDir))
            {
                Program.Logger.EscribirLog("Error temporal al intentar conectar al servidor FTP [" + HostName + "]. Verifique el acceso al servidor FTP y las credenciales.");
                System.Environment.Exit(0);
            }

        }

    }
}
